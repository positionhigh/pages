import * as WebSerial from './webserial.js';
import { _BIGbuffer } from './webserial.js';
// import * as myWebMIDI from './webmidi.js';
import * as Keypad from './keypad.js';
import { basename, int32ToBytes, sortNumbers, sortFiles } from './utils.js';

const FILE_MANAGER = {
	START: 10,
	DIR_ENTRY: 11,
	RECEIVE_HEADER: 12,
	RECEIVE_CHUNK: 13,
	SEND: 14,
	DELETE: 15,
	RENAME: 16,
	END_DIR: 20,
	END_RECEIVE_HEADER: 21,
	END_RECEIVE_CHUNK: 22,
	END_RECEIVE_FILE: 23
};
// const FILE_MANAGER = {
//     SD_DIR : 0x1,
//     SD_SEND : 0x2,
//     SD_RECEIVE : 0x4,
//     SD_DELETE : 0x5,
//     SD_RENAME : 0x6,
//     SD_BACKUP : 0x7,
// };

const fileError = document.querySelector('#fileError');
const progressBar = document.querySelector('#progressBar');
const dxGenerator = document.querySelector('#dxGenerator');

let currentDir = '/';
let _filename;
let link = null;
let playMode = false;

export async function init() {

	await WebSerial.open();

	// File manager modal
	document.querySelector('#btnFileManager').addEventListener('click', async (e) => {
		document.querySelector('.modal-window').classList.toggle('show')
		Keypad.unregister();

		document.dispatchEvent(new Event('fm_opened'));

		currentDir = '/'
		playMode = false;
		progressBar.classList.add('hidden')
		progressBar.querySelector('div').style.width = '0'
		document.querySelector('#sdpath').textContent = currentDir;
		_dir(currentDir);
	}, false)
	document.querySelector('.modal-window .modal-close').addEventListener('click', async (e) => {
		document.querySelector('.modal-window').classList.toggle('show')
		Keypad.register();
		document.dispatchEvent(new Event('fm_closed'));
	}, false)

	// File manager end get directory content event
	document.addEventListener('fm_end_dir', updateDirContent, false);

	// File manager end get file event
	document.addEventListener('fm_end_get', function (e) {
		if (playMode) {
			playFile();
		} else {
			saveFile(_BIGbuffer, _filename)
		}
	}, false);

	// DX bank generator + upload
	dxGenerator.querySelector('button').addEventListener('click', async (e) => {
		progressBar.querySelector('div').style.width = '0'
		dxGenerator.querySelector('.lds-ellipsis').classList.remove('hidden')
		dxGenerator.querySelector('span').classList.add('hidden')

		//https://generate.thisdx7cartdoesnotexist.com
		await fetch('https://local-hero.org/test/testWebSerial/getdxcartridgebank.php')
			.then((response) => response.arrayBuffer())
			.then((data) => {
				// console.log(data)
				document.querySelector('#dxGenerator .lds-ellipsis').classList.add('hidden')
				dxGenerator.querySelector('span').classList.remove('hidden')
				let view = new Uint8Array(data);
				// console.log(view)
				uploadBuffer(view);
			});
	})

}

export async function close() {
	await WebSerial.close();
}

function updateDirContent() {
	let html = '';

	// prepare sort dir names if numbers
	_DirContent.forEach(e => {
		if (e.filename.length == 1 && parseInt(e.filename) != NaN) {
			e.filename = "0" + e.filename
		}
	})

	// sort for display directory content
	if (['/PERFORMANCE', '/DEXED'].includes(currentDir)) {
		_DirContent.sort(sortNumbers)
	}
	if (['/DRUMS', '/CUSTOM'].includes(currentDir)) {
		_DirContent.sort(sortFiles)
	}

	// show bank generator only if we are in a DEXEX pool + bank subfolder
	const regex = /\/DEXED\/\d{1,2}\/\d{1,2}/g;
	document.querySelector('#dxGenerator').classList.add('hidden')
	if (currentDir.match(regex)) {
		document.querySelector('#dxGenerator').classList.remove('hidden')
	}

	// show new content
	_DirContent.forEach(e => {
		if (e.isDir) {
			// dir
			// console.log('DIR', e.filename)

			if (e.filename.length == 2 && parseInt(e.filename) != NaN && e.filename[0] == '0') { // delete leading '0'
				e.filename = e.filename[1]
			}
			html +=
				`<li id="${e.filename}" class="dir">
				<span>${e.filename}</span>
			</li>`;
		} else {
			// file
			// console.log('FILE', e.filename)

			// wav file ?
			let playIcon = '';
			let fileClass = '';
			const fileExtension = e.filename.split('.').pop();
			if (['wav'].includes(fileExtension)) {
				playIcon = `<a class="btn play" href="#" data-file="${e.filename}" title="play">
					<svg width="20" height="20"><use href="#icon-play"></use></svg>
				</a>`;
				fileClass = 'wave'
			}

			html +=
				`<li id="${e.filename}" class="${fileClass}">
				${playIcon}
				<span>${e.filename}</span><input value=${e.filename} class="hidden" />
				<div class="buttons">
					<a class="btn download" href="#" data-file="${e.filename}" title="download">
						<svg width="20" height="20"><use href="#icon-download"></use></svg>
					</a>
					<a class="btn rename ${['json'].includes(fileExtension) ? 'hidden' : ''}" href="#" data-file="${e.filename}" title="rename">
						<svg width="20" height="20"><use href="#icon-edit"></use></svg>
					</a>
					<a class="btn delete ${['json'].includes(fileExtension) ? 'hidden' : ''}" href="#" data-file="${e.filename}" title="delete">
						<svg width="20" height="20"><use href="#icon-delete"></use></svg>
					</a>
				</div>
			</li>`;
		}
	})

	document.querySelector('#filelist').innerHTML += html;

	document.querySelectorAll('#filelist li.dir').forEach(d => d.addEventListener('click', handle_dir_click, false))
	document.querySelectorAll('#filelist li a.play').forEach(f => f.addEventListener('click', handle_play_file_click), false)
	document.querySelectorAll('#filelist li a.download').forEach(f => f.addEventListener('click', handle_get_file_click), false)
	document.querySelectorAll('#filelist li a.delete').forEach(f => f.addEventListener('click', handle_delete_file_click), false)
	document.querySelectorAll('#filelist li a.rename').forEach(f => f.addEventListener('click', handle_rename_file_click), false)
}

function playFile() {
	// console.log('PLAY FILE bigbuffer size', _BIGbuffer.length)

	const audio = new Audio()
	// const blob = new Blob([new Uint8Array(WebSerial._BIGbuffer)], { type: 'audio/wav' })
	const blob = new Blob([new Uint8Array(_BIGbuffer)], { type: 'audio/wav' })
	const url = window.URL.createObjectURL(blob)
	audio.src = url
	audio.play()
}

// function saveFile(fileName) {
// 	console.log('SAVE FILE', fileName)
// }
var saveFile = (function () {
	const a = document.createElement("a");
	a.style = "display: none";
	document.body.appendChild(a);
	return async function (data, fileName) {
		const get_url_base64_arraybuffer = async (data) => {
			return await new Promise((r) => {
				const reader = new FileReader()
				reader.onload = () => r(reader.result)
				reader.readAsDataURL(new Blob([new Uint8Array(data)]))
			})
		}

		const url = await get_url_base64_arraybuffer(data);
		a.href = url;
		a.download = fileName;
		a.click();
		window.URL.revokeObjectURL(url);
	};
}());

function handle_play_file_click(e) {
	e.preventDefault();
	e.stopPropagation();
	playMode = true;
	// console.log(e.currentTarget.parentNode)
	document.querySelector('#filelist li.current')?.classList.remove('current')
	e.currentTarget.parentNode.classList.add('current')

	if (currentDir == '/') {
		_get(`/${e.currentTarget.dataset['file']}`);
	} else {
		_get(`${currentDir}/${e.currentTarget.dataset['file']}`);
	}
}

function handle_get_file_click(e) {
	e.preventDefault();
	e.stopPropagation();

	link = e.currentTarget;
	playMode = false;

	if (currentDir == '/') {
		_get(`/${e.currentTarget.dataset['file']}`);
	} else {
		_get(`${currentDir}/${e.currentTarget.dataset['file']}`);
	}
}

function handle_delete_file_click(e) {
	e.preventDefault();
	e.stopPropagation();

	if (currentDir == '/') {
		_delete(`/${e.currentTarget.dataset['file']}`);
	} else {
		_delete(`${currentDir}/${e.currentTarget.dataset['file']}`);
	}

	// refresh directory
	_dir(currentDir);
}

function handle_rename_file_click(e) {
	e.preventDefault();
	e.stopPropagation();

	const spanFile = e.currentTarget.closest('li').querySelector('span')
	spanFile.classList.add('hidden')

	const editInput = e.currentTarget.closest('li').querySelector('input')
	editInput.focus()
	editInput.classList.remove('hidden')

	const filename = e.currentTarget.dataset['file']
	editInput.value = filename.split('.').slice(0, -1).join('.')
	const fileExt = filename.split('.').pop()

	editInput.addEventListener('blur', async (e) => {
		spanFile.classList.remove('hidden')
		editInput.classList.add('hidden')
		const newFilename = `${editInput.value}.${fileExt}`

		if (currentDir == '/') {
			_rename(`/${filename}`, `/${newFilename}`);
		} else {
			_rename(`${currentDir}/${filename}`, `${currentDir}/${newFilename}`);
		}

		// refresh directory
		_dir(currentDir);
	})
}

function handle_dir_click(e) {
	if (e.currentTarget.id == 'back') {
		currentDir = currentDir.slice(0, currentDir.lastIndexOf('/'))
		if (currentDir == '') currentDir = '/'
	} else {
		if (currentDir == '/') {
			currentDir += e.currentTarget.id;
		} else {
			currentDir += '/' + e.currentTarget.id;
		}
	}

	if (currentDir == '/') {
		document.querySelector('#back').classList.add('hidden')
	} else {
		document.querySelector('#back').classList.remove('hidden')
	}

	document.querySelector('#sdpath').textContent = currentDir;
	_dir(currentDir);
}

// upload with file input
document.querySelector('#fileElem').addEventListener('change', handleFileInput, false);

function handleFileInput() {
	progressBar.querySelector('div').style.width = '0'
	handleFiles(this.files)
}

// upload & drag-drop
let dropArea = document.getElementById('drop-area');
;['dragenter', 'dragover', 'dragleave', 'drop'].forEach(eventName => {
	dropArea.addEventListener(eventName, preventDefaults, false)
})

function preventDefaults(e) {
	e.preventDefault()
	e.stopPropagation()
}

;['dragenter', 'dragover'].forEach(eventName => {
	dropArea.addEventListener(eventName, highlight, false)
})
	;['dragleave', 'drop'].forEach(eventName => {
		dropArea.addEventListener(eventName, unhighlight, false)
	})

function highlight(e) {
	dropArea.classList.add('highlight')
}

function unhighlight(e) {
	dropArea.classList.remove('highlight')
}

dropArea.addEventListener('drop', handleDrop, false)

function handleDrop(e) {
	let dt = e.dataTransfer
	let files = dt.files
	fileError.classList.add('hidden')
	fileError.innerHTML = ''

	handleFiles(files)
}

function handleFiles(files) {
	([...files]).forEach(uploadFile)
}

let chunkCounter = 0;
const chunkSize = 512;
let chunkEnd = 0;

async function uploadFile(file) {

	// check file extension
	const validExt = ["wav", "json", "syx", "mid"];
	if (!validExt.includes(file.name.split('.').pop())) {
		fileError.innerHTML = 'File extension not accepted (only .wav, .syx, .mid and .json)'
		fileError.classList.remove('hidden')
		return
	}

	// check file size
	const fileMb = file.size / 1024 ** 2;
	if (fileMb > 5) {
		fileError.innerHTML = "Please select a file less than 5MB.";
		fileError.classList.remove('hidden')
		// fileSubmit.disabled = true;
		return
	}

	// all ok
	fileError.classList.add('hidden')
	progressBar.classList.remove('hidden')
	progressBar.querySelector('div').style.width = '0'

	const path = `${currentDir == '/' ? '' : currentDir}/${file.name}`
	_send(path)

	const numberofChunks = Math.ceil(file.size / chunkSize);
	const progressChunk = Math.floor(numberofChunks / 20);
	const progressChunkPixel = Math.floor(progressBar.clientWidth / 20);

	let start = 0;
	chunkCounter = 0;
	chunkEnd = start + chunkSize;

	// send file size on 4 bytes
	WebSerial.send(int32ToBytes(file.size));

	// send chunks
	for (let i = 0; i < numberofChunks; i++) {
		await createChunk(file, start);
		start += chunkSize;

		if (i % progressChunk == 0) {
			progressBar.querySelector('div').style.width = `${i / progressChunk * progressChunkPixel}px`
		}
		// setTimeout( () => {}, 20);
	}

	progressBar.querySelector('div').style.width = '99.9%'
	setInterval(1000, function () {
		// progressBar.querySelector('div').style.display = 'none';
		progressBar.classList.add('hidden')
	})

	// refresh directory
	_dir(currentDir);
}

async function createChunk(data, start, end) {
	chunkCounter++;
	chunkEnd = Math.min(start + chunkSize, data.size);
	const chunk = data.slice(start, chunkEnd);
	console.log(`chunk #${chunkCounter} : ${start} - ${chunkEnd} minus 1`);
	console.log('chunk size:', chunk.size)
	// WebSerial.sendString(String.fromCharCode(chunk.size)+String.fromCharCode(chunk.size));

	let arrayBuffer = await chunk.arrayBuffer();
	WebSerial.send(arrayBuffer);
}


async function uploadBuffer(data) {

	fileError.classList.add('hidden')
	progressBar.classList.remove('hidden')
	progressBar.querySelector('div').style.width = '0'

	let randomFile = (Math.random() + 1).toString(36).substring(7);
	const path = `${currentDir == '/' ? '' : currentDir}/gen${randomFile}.syx`
	_send(path)

	const numberofChunks = Math.ceil(data.length / chunkSize);
	const progressChunk = Math.floor(numberofChunks / 20);
	const progressChunkPixel = Math.floor(progressBar.clientWidth / 20);

	let start = 0;
	chunkEnd = start + chunkSize;

	// send buffer size on 4 bytes
	WebSerial.send(int32ToBytes(data.length));

	// send chunks
	for (let i = 0; i < numberofChunks; i++) {
		await createChunkBuffer(data, start);
		start += chunkSize;

		if (i % progressChunk == 0) {
			progressBar.querySelector('div').style.width = `${i / progressChunk * progressChunkPixel}px`
		}
		// setTimeout( () => {}, 20);
	}

	progressBar.querySelector('div').style.width = '99.9%'
	// progressBar.classList.add('hidden')

	// refresh directory
	_dir(currentDir);
}

async function createChunkBuffer(data, start, end) {
	chunkCounter++;
	chunkEnd = Math.min(start + chunkSize, data.length);
	const chunk = data.slice(start, chunkEnd);

	WebSerial.send(chunk);
}


///////////////////////////////////////////////////////////////
let sysexFilepath;

function _dir(dirname) {
	// clean file list
	_DirContent = [];
	document.querySelectorAll('#filelist li:not(#back)').forEach(dir => dir.remove())

	// remove listeners
	document.querySelectorAll('#filelist li.dir').forEach(d => d.removeEventListener('click', handle_dir_click))
	document.querySelectorAll('#filelist li a.download').forEach(f => f.removeEventListener('click', handle_get_file_click))
	document.querySelectorAll('#filelist li a.delete').forEach(f => f.removeEventListener('click', handle_delete_file_click))

	// let sysexFilepath = Uint8Array.from(Array.from(dirname).map(letter => letter.charCodeAt(0)));
	// myWebMIDI.sendSysex([FILE_MANAGER.SD_DIR, ...dir]);
	// myWebMIDI.sendSysex([0x1, ...sysexFilepath]);

	WebSerial.sendString(`%dir!${dirname}!%`);
}

function _get(filepath) {
	_filename = basename(filepath);
	WebSerial.sendString(`%get!${filepath}!%`);

	// sysexFilepath = Uint8Array.from(Array.from(filepath).map(letter => letter.charCodeAt(0)));
	// myWebMIDI.sendSysex([0x2, ...sysexFilepath]);
}

function _send(filepath) {
	WebSerial.sendString(`%put!${filepath}!%`);

	// sysexFilepath = Uint8Array.from(Array.from(filepath).map(letter => letter.charCodeAt(0)));
	// myWebMIDI.sendSysex([0x4, ...sysexFilepath]);
}

function _delete(filepath) {
	WebSerial.sendString(`%delete!${filepath}!%`);

	// sysexFilepath = Uint8Array.from(Array.from(filepath).map(letter => letter.charCodeAt(0)));
	// myWebMIDI.sendSysex([0x5, ...sysexFilepath]);
}

function _rename(filepath, newfilePath) {
	WebSerial.sendString(`%rename!${filepath}|${newfilePath}|!%`);

	// let sysexOldName = Uint8Array.from(Array.from(filepath).map(letter => letter.charCodeAt(0)));
	// let sysexNewName = Uint8Array.from(Array.from(newfilePath).map(letter => letter.charCodeAt(0)));

	// myWebMIDI.sendSysex([0x6, ...sysexOldName, 124, ...sysexNewName, 124]); // 124 == '|'
}


// let _BIGbuffer = [];
// window._DirContent = [];
// let filesize = 0;
// let filesize_check = 0;
// let nbChunk = 1;

// export function fm_processSysex(chunk) {
// 	// console.log('FM SYSEX', chunk)

// 	switch (chunk[1]) {
// 		// case FILE_MANAGER.START: // start
// 		// 	// console.log('FM SYSEX START')
// 		// 	break;

// 		case FILE_MANAGER.DIR_ENTRY: // directory content
// 			filesize = 0;
// 			let isDir = false;
// 			let filename;

// 			// 64 is ascii code for '@' added on directory's filename on MDT
// 			if (chunk[chunk.length - 1] == 64) {
// 				// dir
// 				filename = chunk.slice(2, chunk.length - 1);
// 				isDir = true;
// 			} else {
// 				filename = chunk.slice(2, chunk.length);
// 			}

// 			// console.log('FM SYSEX DIR: ', isDir ? 'DIR' : 'FILE', String.fromCharCode.apply(String, filename))

// 			_DirContent.push({ filename: String.fromCharCode.apply(String, filename), filesize: filesize, isDir: isDir })
// 			break;

// 		case FILE_MANAGER.RECEIVE_HEADER:
// 			_BIGbuffer = [];
// 			nbChunk = 0;
// 			filesize = 0;
// 			filesize_check = 0;

// 			// get filesize as five 7bits byte
// 			for (let i = 0; i < 5; i++) {
// 				filesize |= (chunk[2 + i] & 0x7F) << (7 * i); // Kepp only the 7 bits
// 			}

// 			// console.log('FM GETFILE filesize', filesize)
// 			break;

// 		case FILE_MANAGER.RECEIVE_CHUNK:
// 			// console.log('FM GETFILE CHUNK', chunk)
// 			// console.log('FM GETFILE CHUNK size', chunk.length)

// 			// convert chunk 7bits bytes into "real" bytes
// 			// start at position 2 to keep only sample data
// 			for (let i = 2; i < chunk.length; i++) {
// 				_BIGbuffer.push(chunk[i] * 128 + chunk[i + 1]);
// 				i++;
// 			}

// 			filesize_check += (chunk.length - 2) / 2;
// 			nbChunk++
// 			break;

// 		case FILE_MANAGER.SEND:
// 			console.log('FM SENDFILE not yet available')
// 			break;

// 		case FILE_MANAGER.END_DIR: // end
// 			// console.log('FM SYSEX END_DIR')
// 			document.dispatchEvent(new Event('fm_end_dir'));
// 			break;

// 		case FILE_MANAGER.END_RECEIVE_FILE: // end receive file
// 			// console.log('FM SYSEX END_RECEIVE_FILE')

// 			if (filesize != filesize_check) {
// 				console.error(`DOWNLOAD PROBLEM: wanted ${filesize}, received: ${filesize_check}`)
// 				document.querySelector('#filelist li.current').classList.add('error')
// 			} else {
// 				console.log(`DOWNLOAD OK size: ${filesize_check} - chunks: ${nbChunk}`)
// 				document.dispatchEvent(new Event('fm_end_get'));
// 			}
// 			break;

// 		default:
// 			console.log('FM DEFAULT - nothing to do', chunk)
// 			break;
// 	}
// }