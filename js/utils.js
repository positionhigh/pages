const palette = {
    "0": 0x0000,
    "1": 0xFFFF,
    "2": 0x7BBD,
    "3": 0xE2FA,
    "4": 0xFC80,
    "5": 0xFE4F,
    "6": 0x159A,
    "7": 0xD000,
    "8": 0xF81F,
    "9": 0xFFEB,
    "10": 0x07E0,
    "11": 0x1500,
    "12": 0x0AE0,
    "13": 0xC638,
    "14": 0x52AA,
    "15": 0x2104,
    "16": 0x10A2,
    "17": 0x030D,
}

export function sortNumbers(a, b) {
    const nameA = parseInt(a.filename);
    const nameB = parseInt(b.filename);

    if (nameA < nameB) {
        return -1;
    }
    if (nameA > nameB) {
        return 1;
    }

    // names must be equal
    return 0;
}

export function sortFiles(a, b) {
    const nameA = (a.isDir ? '_' : '') + a.filename.toLowerCase();
    const nameB = (b.isDir ? '_' : '') + b.filename.toLowerCase();

    if (nameA < nameB) {
        return -1;
    }
    if (nameA > nameB) {
        return 1;
    }

    // names must be equal
    return 0;
}

export function clear(selector) {
    document
        .querySelectorAll(selector)
        .forEach(e => e.innerHTML = '');
}

export function basename(path) {
    return path.replace(/.*\//, '');
}

export function show(selector) {
    document
        .querySelectorAll(selector)
        .forEach(e => e.classList.remove('hidden'));
}

export function removeClass(selector, className) {
    document
        .querySelectorAll(selector)
        .forEach(e => e.classList.remove(className));
}
export function hide(selector) {
    document
        .querySelectorAll(selector)
        .forEach(e => e.classList.add('hidden'));
}

export function toggle(selector) {
    document
        .querySelectorAll(selector)
        .forEach(e => e.classList.contains('hidden')
            ? e.classList.remove('hidden')
            : e.classList.add('hidden'));
}
export function toggleClass(e, className) {
    e.classList.contains(className)
        ? e.classList.remove(className)
        : e.classList.add(className);
}


export function wait(time) {
    return new Promise(resolve => setTimeout(resolve, time));
}

export function appendButton(target, title, onClick) {
    const button = document.createElement('button');
    button.innerText = title;
    on(button, 'click', onClick);

    if (typeof target === 'string') {
        target = document.querySelector(target)
    }

    target.append(button);

    return button;
}

export function on(target, eventType, action, useCapture) {
    if (typeof target === 'string') {
        target = document.querySelectorAll(target);
    } else if (!(target instanceof Array)) {
        target = [target];
    }

    for (const element of target) {
        element.addEventListener(eventType, action, useCapture);
    }
}

export function off(target, eventType, action, useCapture) {
    target.removeEventListener(eventType, action, useCapture);
}

export function int32ToBytes(intVal) {
    return [
        intVal & 0xff,
        (intVal >> 8) & 0xff,
        (intVal >> 16) & 0xff,
        (intVal >> 24) & 0xff
    ]
}

export function bitRead(b, bitPos) {
    const x = b & (1 << bitPos);
    return x == 0 ? 0 : 1;
}

export function getRGBColor(data) {
    let color;

    if (data.length == 1) {
        // known colors
        color = palette[data];
    } else {
        // color sent on 4 MIDI 7bits bytes
        color = get8bitValue(data, 0) * 256 + get8bitValue(data, 2);
        // console.log('getcolor', color);
    }

    const b = (((color) & 0x001F) << 3) & 0xFF;
    const g = (((color) & 0x07E0) >>> 3) & 0xFF;
    const r = (((color) & 0xF800) >>> 8) & 0xFF;
    return `rgb(${r},${g},${b})`;
}

export function get8bitValue(data, start) {
    return data[start] * 128 + data[start + 1];
}
