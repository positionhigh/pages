import * as MediaRecorder from './mediarecorder.js';
import { toggleClass } from './utils.js';

// Web Audio
let ctxAudio;
let streamAudio;

const WIDTH = 240;
const HEIGHT = 70;

export function feature() {
	// feature detection WebAudio
	try {
		if (window.AudioContext) {
			document.querySelector('#featureAudio').classList.add('active');
		}
		return true;
	}
	catch (e) {
		alert('Web Audio feature is not available in this browser');
	}
}

export async function init() {
	try {
		// start web audio
		try {
			streamAudio = await start()
		} catch {
			alert('WebAudio bad configuration');
		}

		return streamAudio
	}
	catch (e) {
		alert('Web Audio feature is not available in this browser');
	}
}

async function start(attempts = 1) {
	if (ctxAudio) return streamAudio;

	try {
		const optionsAudioCtx = {
			// latencyHint: 'interactive',
			// latencyHint: 'playback',
			latencyHint: 0,
			// latencyHint: 0.5,
			// sampleRate: 44100
		}
		await navigator.mediaDevices.getUserMedia({ audio: true, video: false });
		ctxAudio = new AudioContext(optionsAudioCtx);
		// console.log('audio context sample rate', ctxAudio.sampleRate);

		let deviceId;
		while (true) {
			deviceId = await findAudioDeviceId();
			if (deviceId)
				break;

			if (--attempts > 0) {
				await sleep(300);
			} else {
				break;
			}
		}

		if (!deviceId)
			throw new Error('MicroDexed not found');

		streamAudio = await navigator.mediaDevices
			.getUserMedia({
				audio: {
					deviceId: { exact: deviceId },
					autoGainControl: false,
					echoCancellation: false,
					noiseSuppression: false,
					// latency: 0,
					// channelCount: 2,
					// sampleSize: 16,
					// sampleRate: 44100
				}
			})

		const source = ctxAudio.createMediaStreamSource(streamAudio);
		const analyser = ctxAudio.createAnalyser();
		const analyserL = ctxAudio.createAnalyser();
		const analyserR = ctxAudio.createAnalyser();
		const analyser2 = ctxAudio.createAnalyser();

		analyser.smoothingTimeConstant = 0.2;
		analyser2.smoothingTimeConstant = 0.2;

		source.connect(ctxAudio.destination);
		// const gainNode = ctxAudio.createGain();
		// gainNode.gain.setValueAtTime(0, audioContext.currentTime);
		// source.connect(gainNode);
		// gainNode.connect(audioContext.destination);

		source.connect(analyser);

		const splitter = ctxAudio.createChannelSplitter(2);
		// const merger = ctxAudio.createChannelMerger(2);

		analyser.connect(splitter);
		splitter.connect(analyserL, 0);
		splitter.connect(analyserR, 1);

		analyser.connect(analyser2);

		// function draw() {
		// 	requestAnimationFrame(draw);

		// 	// FFT
		// 	analyser.fftSize = 2048;
		// 	const bufferLength = analyser.frequencyBinCount;
		// 	const dataArray = new Uint8Array(bufferLength);
		// 	analyser.getByteTimeDomainData(dataArray);
		// 	let canvasCtx = document.querySelector('#analyser').getContext('2d', { alpha: false });
		// 	// canvasCtx.clearRect(0, 0, 640, 240);

		// 	canvasCtx.fillStyle = "#181818";
		// 	canvasCtx.fillRect(0, 0, WIDTH, HEIGHT);

		// 	canvasCtx.lineWidth = 2;
		// 	// canvasCtx.strokeStyle = "#02f72f";
		// 	canvasCtx.strokeStyle = "#10b0d0";
		// 	canvasCtx.beginPath();
		// 	const sliceWidth = WIDTH / bufferLength;
		// 	let x = 0;
		// 	for (let i = 0; i < bufferLength; i++) {
		// 		const v = dataArray[i] / 128.0;
		// 		const y = v * (HEIGHT / 2);

		// 		if (i === 0) {
		// 			canvasCtx.moveTo(x, y);
		// 		} else {
		// 			canvasCtx.lineTo(x, y);
		// 		}

		// 		x += sliceWidth;
		// 	}
		// 	canvasCtx.lineTo(WIDTH, HEIGHT / 2);
		// 	canvasCtx.stroke();
		// }
		function drawLR() {
			const drawVisual = requestAnimationFrame(drawLR);

			// FFT
			analyserL.fftSize = 2048;
			analyserR.fftSize = 2048;
			const bufferLengthL = analyserL.frequencyBinCount;
			const dataArrayL = new Uint8Array(bufferLengthL);
			analyserL.getByteTimeDomainData(dataArrayL);

			const bufferLengthR = analyserR.frequencyBinCount;
			const dataArrayR = new Uint8Array(bufferLengthR);
			analyserR.getByteTimeDomainData(dataArrayR);

			let canvasL = document.querySelector('#analyserL').getContext('2d', { alpha: false });

			// left channel
			canvasL.fillStyle = "#181818";
			canvasL.fillRect(0, 0, WIDTH, HEIGHT);

			canvasL.lineWidth = 2;
			canvasL.strokeStyle = "#10b0d0";
			canvasL.beginPath();
			const sliceWidth = WIDTH / bufferLengthL;
			let x = 0;
			for (let i = 0; i < bufferLengthL; i++) {
				const v = dataArrayL[i] / 128.0;
				const y = v * (HEIGHT / 2);

				if (i === 0) {
					canvasL.moveTo(x, y);
				} else {
					canvasL.lineTo(x, y);
				}

				x += sliceWidth;
			}
			canvasL.lineTo(WIDTH, HEIGHT / 2);
			canvasL.stroke();

			// right channel
			{
				let canvasR = document.querySelector('#analyserR').getContext('2d', { alpha: false });
				canvasR.fillStyle = "#181818";
				canvasR.fillRect(0, 0, WIDTH, HEIGHT);

				canvasR.strokeStyle = "red";
				canvasR.beginPath();
				const sliceWidth = WIDTH / bufferLengthR;
				let x = 0;
				for (let i = 0; i < bufferLengthR; i++) {
					const v = dataArrayR[i] / 128.0;
					const y = v * (HEIGHT / 2);

					if (i === 0) {
						canvasR.moveTo(x, y);
					} else {
						canvasR.lineTo(x, y);
					}

					x += sliceWidth;
				}
				canvasR.lineTo(WIDTH, HEIGHT / 2);
				canvasR.stroke();
			}
		}
		// function drawBar() {
		// 	const drawVisual = requestAnimationFrame(drawBar);

		// 	analyser.fftSize = 256;
		// 	const bufferLength = analyser.frequencyBinCount;
		// 	const dataArray = new Uint8Array(bufferLength);
		// 	analyser.getByteFrequencyData(dataArray);

		// 	let canvasBar = document.querySelector('#analyser2');
		// 	canvasBar.width = WIDTH;
		// 	canvasBar.height = HEIGHT;
		// 	let canvasCtx = canvasBar.getContext('2d', { alpha: false });
		// 	// canvasCtx.clearRect(0, 0, 640, 240);

		// 	canvasCtx.fillStyle = "#181818";
		// 	canvasCtx.fillRect(0, 0, WIDTH, HEIGHT);

		// 	const barWidth = (WIDTH / bufferLength) * 2.5;
		// 	let barHeight;
		// 	let x = 0;
		// 	for (let i = 0; i < bufferLength; i++) {
		// 		barHeight = dataArray[i] * 1.75;

		// 		canvasCtx.fillStyle = `rgb(${barHeight + 100}, 50, 50)`;
		// 		canvasCtx.fillRect(x, 240 - barHeight / 2, barWidth, barHeight);

		// 		x += barWidth + 1;
		// 	}
		// }

		// function draw3() {
		// 	requestAnimationFrame(draw3);

		// 	// Interesting parameters to tweak!
		// 	const SMOOTHING = 0.5;
		// 	const FFT_SIZE = 512;

		// 	analyser2.smoothingTimeConstant = SMOOTHING;
		// 	analyser2.minDecibels = -140;
		// 	analyser2.maxDecibels = 0;

		// 	// Get the frequency data from the currently playing music
		// 	analyser2.fftSize = 2048;
		// 	const freqs = new Uint8Array(analyser2.frequencyBinCount);
		// 	// analyser2.fftSize = 512;
		// 	const times = new Uint8Array(analyser2.frequencyBinCount);

		// 	analyser2.getByteFrequencyData(freqs);
		// 	analyser2.getByteTimeDomainData(times);

		// 	let width = Math.floor(1 / freqs.length, 10);

		// 	let drawContext = document.querySelector('#analyser3').getContext('2d');
		// 	drawContext.fillStyle = "#181818";
		// 	drawContext.fillRect(0, 0, WIDTH, HEIGHT);
		// 	// canvas.width = WIDTH;
		// 	// canvas.height = HEIGHT;

		// 	// Draw the frequency domain chart.
		// 	for (let i = 0; i < analyser2.frequencyBinCount; i++) {
		// 		let value = freqs[i];
		// 		let percent = value / 256;
		// 		let height = HEIGHT * percent;
		// 		let offset = HEIGHT - height - 1;
		// 		if (value < 10) {
		// 			percent = 0;
		// 			offset = 0;
		// 			height = 0;
		// 		}
		// 		let barWidth = WIDTH / analyser2.frequencyBinCount;
		// 		let hue = i / analyser2.frequencyBinCount * 360;
		// 		// drawContext.fillStyle = 'hsl(' + hue + ', 100%, 50%)';
		// 		// drawContext.fillStyle = '#119911';
		// 		const gradient = drawContext.createLinearGradient(0, 0, 0, height);
		// 		gradient.addColorStop(1, "#10b0d0");
		// 		// gradient.addColorStop(0.2, "#119911");
		// 		gradient.addColorStop(0, "green");
		// 		drawContext.fillStyle = gradient;
		// 		drawContext.fillRect(i * barWidth, offset, barWidth, height);
		// 	}

		// 	// // Draw the time domain chart.
		// 	// for (var i = 0; i < analyser2.frequencyBinCount; i++) {
		// 	// 	var value = times[i];
		// 	// 	var percent = value / 256;
		// 	// 	var height = HEIGHT * percent;
		// 	// 	var offset = HEIGHT - height - 1;
		// 	// 	var barWidth = WIDTH/analyser2.frequencyBinCount;
		// 	// 	drawContext.fillStyle = 'white';
		// 	// 	drawContext.fillRect(i * barWidth, offset, 1, 2);
		// 	// }
		// }

		// draw();
		drawLR();
		// drawBar();
		// draw3();

		if (ctxAudio.state !== 'running') {
			ctxAudio && ctxAudio.resume();
		}

	} catch (err) {
		console.error(err);
		stopAudio();

		return null;
	}

	// if (!audioEnabled) {
	//     stopAudio();
	// }

	return streamAudio;
}

// btnAudio.addEventListener('click', async () => {
// 	if (!ctxAudio) {
// 		btnAudio.textContent = 'Stop audio'
// 		startAudio(10);
// 	} else {
// 		btnAudio.textContent = 'Start audio'
// 		stopAudio();
// 	}
// });

async function findAudioDeviceId() {
	const devices = await navigator.mediaDevices.enumerateDevices();
	// console.log(devices)
	return devices
		.filter(d =>
			d.kind === 'audioinput' &&
			// (/MicroDexed/.test(d.label) || /Teensy MIDI\/Audio/.test(d.label)) &&
			// d.deviceId !== 'default' &&
			// d.deviceId !== 'communications'
			/MicroDexed/.test(d.label)
		)
		.map(d => d.deviceId)[0];
}

export async function stop() {
	ctxAudio && await ctxAudio.close().catch(() => { });
	ctxAudio = null;
}
