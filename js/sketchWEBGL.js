// Sketch written for p5js
import * as myWebMIDI from './webmidi.js';
import * as MediaRecorder from './mediarecorder.js';
import { renderChunk, WIDTH, HEIGHT, scale } from './renderWEBGL.js';
import { toggleClass } from './utils.js';

const debug = false;
const fps = 30;

let myP5Instance;
let dexedMidiInput;
let streamAudio;
let recording = false;

let btnRecordAudio = document.querySelector('#btnRecordAudio');
let btnRecordVideo = document.querySelector('#btnRecordVideo');

export const init = (aStreamAudio, aSynthMidiInput,) => {
    streamAudio = aStreamAudio;
    dexedMidiInput = aSynthMidiInput;

    myP5Instance = new p5(sketch, 'renderZone');
}

export const clear = () => {
    myP5Instance.clear();
    myP5Instance.remove();

    myWebMIDI.sendCloseRemoteDisplay();
}


const sketch = p => {
    let myFont;
    p.preload = function () {
        myFont = p.loadFont('css/mdt5x7.woff2');
    }

    p.clear = function () {
        // remove listeners
        dexedMidiInput.removeListener('sysex', handle_sysex);
        document.querySelector('#btnSnapshot').removeEventListener('click', handle_capture_image);
        btnRecordAudio.removeEventListener('click', handle_capture_audio);
        btnRecordVideo.removeEventListener('click', handle_capture_video);
    }

    p.setup = function () {
        p.createCanvas(WIDTH, HEIGHT, p.WEBGL);
        p.translate(-p.width / 2, -p.height / 2, 0);
        p.background(0);
        p.textFont(myFont);

        // Redraw order for MDT
        myWebMIDI.sendInitRemoteDisplay();

        // Listener on sysex receive
        dexedMidiInput.addListener("sysex", handle_sysex);

        // photo capture
        document.querySelector('#btnSnapshot').addEventListener('click', handle_capture_image);

        // video capture
        p.frameRate(fps);
        btnRecordVideo.addEventListener('click', handle_capture_video);

        // audio capture
        btnRecordAudio.addEventListener("click", handle_capture_audio);

        // file manager
        document.addEventListener('fm_opened', function () {
            console.log('fm_opened')
            disable_touch_mouse();
        });
        document.addEventListener('fm_closed', function () {
            console.log('fm_closed')
            enable_touch_mouse();
        });

        // enbale touch/mouse by default
        enable_touch_mouse();


        // Listeners for keyboard shortcuts
        // document.onkeyup = function (e) {
        //     console.log('sketch keyup', e)
        //     // if (e.altKey && e.key == 'd') {
        //     //     callback_disconnect();
        //     // }
        //     if (e.altKey && e.key == 'i') {
        //         handle_capture_image();
        //     }
        //     if (e.altKey && e.key == 'v') {
        //         handle_capture_video();
        //     }
        //     if (e.altKey && e.key == 'a') {
        //         handle_capture_audio();
        //     }
        //     else {
        //         console.log('no key mapped')
        //     }
        // };

    } // end setup

    p.draw = function () {
        p.translate(-p.width / 2, -p.height / 2, 0);
    }

    function enable_touch_mouse() {
        // Mouse/touch events handlers
        p.mousePressed = function () {
            handle_touch_mouse_pressed();
        }
        p.touchStarted = function () {
            handle_touch_mouse_pressed();
        }

        p.mouseReleased = function () {
            handle_touch_mouse_released();
        }
        p.touchEnded = function () {
            handle_touch_mouse_released();
        }

        p.mouseWheel = function (event) {
            handle_mouse_wheel(event)
        }
    }
    function disable_touch_mouse() {
        // Mouse/touch events handlers
        p.mousePressed = function () { }
        p.touchStarted = function () { }
        p.mouseReleased = function () { }
        p.touchEnded = function () { }
        p.mouseWheel = function () { }
    }

    function handle_touch_mouse_pressed(e) {
        // simulate touch pressed
        if (p.mouseX < 0 || p.mouseX > p.width || p.mouseY < 0 || p.mouseY > p.height) return;

        let x = Math.round((p.mouseX) / scale); // divide by scale because of canvas scaled to 640*480
        let y = Math.round((p.mouseY) / scale);
        // console.log(`handle_touch_mouse_pressed x ${x} y ${y}`)

        myWebMIDI.sendTouchDisplay(Math.floor(x / 2.5) - 1, Math.floor(y / 2.5) - 1);
    }
    function handle_touch_mouse_released(e) {
        if (p.mouseX < 0 || p.mouseX > p.width || p.mouseY < 0 || p.mouseY > p.height) return;

        // console.log(`handle_touch_mouse_released`)
        myWebMIDI.sendTouchRelease();
    }

    function handle_mouse_wheel(event) {
        if (event.deltaY < 0) {
            myWebMIDI.sendCommandOn("up")
        } else {
            myWebMIDI.sendCommandOn("down")
        }
    }

    // capture
    function handle_capture_image() {
        p.saveCanvas('mdt_screenshot', 'jpg');
    }

    function handle_capture_video() {
        toggleClass(btnRecordVideo, 'active');

        if (!recording) {
            btnRecordVideo.innerHTML = "Stop <u>v</u>ideo";
            MediaRecorder.startVideo(streamAudio, fps);
        } else {
            btnRecordVideo.innerHTML = "Record <u>v</u>ideo";
            MediaRecorder.stopVideo();
        }

        recording = !recording;
    }

    function handle_capture_audio() {
        toggleClass(btnRecordAudio, 'active');

        if (!recording) {
            btnRecordAudio.innerHTML = "Stop <u>a</u>udio";
            MediaRecorder.startAudio(streamAudio);
        } else {
            btnRecordAudio.innerHTML = "Record <u>a</u>udio";
            MediaRecorder.stopAudio(streamAudio);
        }

        recording = !recording;
    }

    function handle_sysex(e) {
        // console.log('SYSEX received, data', e.data);
        processSysex(e.data)
    }

    // render from SYSEX message
    function processSysex(data) {
        // MIDI bundled sysex
        // MDT remote sysex header is 3 bytes : 0xf0 0x43 0x21
        if (data[2] != 0x21) {
            console.error('NON REMOTE SYSEX', data)
            return // non remote sysex
        }

        // MDT rendering sysex
        data = data.slice(3, data.length - 1); // trim MIDI sysex header (3 bytes) and last MIDI sysex byte 0xf7
        if (debug) console.log(`data ${data}`)

        // browse the MIDI bundled sysex message
        while (data.length) {
            // get chunk
            const length = data[0];
            const chunk = data.slice(1, length + 1);

            // process chunk
            if (length > 0) {
                renderChunk(p, chunk);
            }

            // pop chunk from data
            data = data.slice(length + 1);
            if (debug) console.log(`new data [${data}]`)
        }
    }

}; // end sketch
