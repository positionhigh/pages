let mediaRecorder;
let recordedChunks;
let combined;

export function startVideo(streamAudio, fps) {
    // chunks.length = 0;
    recordedChunks = [];

    // 		const streamVideo = display.captureStream(30);
    const streamVideo = document.querySelector('#defaultCanvas0').captureStream(fps)

    if (streamAudio) {
        combined = new MediaStream([...streamVideo.getTracks(), ...streamAudio.getTracks()]);
    } else {
        combined = streamVideo;
    }

    mediaRecorder = new MediaRecorder(combined, {
        audioBitsPerSecond: 128000,
        videoBitsPerSecond: 2500000,
        mimeType: 'video/webm;codecs=vp9',
        // mimeType: 'video/mp4',
        ignoreMutedMedia: true
    });
    mediaRecorder.ondataavailable = e => {
        if (e.data.size > 0) {
            recordedChunks.push(e.data);
        }
    };
    mediaRecorder.start();
}

export function stopVideo() {

    mediaRecorder.stop();
    setTimeout(() => {
        const blob = new Blob(recordedChunks, {
            type: 'video/webm'
        });

        // Download the video 
        const url = URL.createObjectURL(blob);
        const a = document.createElement("a");
        a.href = url;
        a.download = "mdt_video.webm";
        // a.download = "mdt_video.mp4";
        a.click();
        URL.revokeObjectURL(url);
    }, 0);
}

export function startAudio(streamAudio) {
    recordedChunks = [];

    const optionsAudioMedia = {
        // audioBitsPerSecond: 128000,
        // mimeType: 'audio/ogg; codecs=opus'
    };
    mediaRecorder = new MediaRecorder(streamAudio, optionsAudioMedia);
    mediaRecorder.ondataavailable = e => {
        if (e.data.size > 0) {
            recordedChunks.push(e.data);
        }
    };
    mediaRecorder.start();
}

export function stopAudio() {
    mediaRecorder.stop();
    setTimeout(() => {
        const blob = new Blob(recordedChunks, {
            // type: "audio/ogg; codecs=opus"
            type: "audio/webm"
        });
        const url = URL.createObjectURL(blob);
        const a = document.createElement("a");
        a.href = url;
        a.download = "mdt_audio.weba";
        a.click();
        URL.revokeObjectURL(url);
    }, 0);
}