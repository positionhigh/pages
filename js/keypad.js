import * as myWebMIDI from './webmidi.js';
import { removeClass } from './utils.js';

export const keyboard = document.querySelector('#keyboard');
const container = document.querySelector('.container');
let autolearn = false;
let keyedit = null;

const defaultKeyboardMappings = {
	"ArrowRight": "right",
	"Numpad6": "right",
	"KeyD": "right",
	"ArrowLeft": "left",
	"Numpad4": "left",
	"KeyA": "left",
	"ArrowUp": "up",
	"Numpad8": "up",
	"KeyW": "up",
	"ArrowDown": "down",
	"Numpad2": "down",
	"KeyS": "down",
	"ShiftLeft": "select",
	"NumpadMultiply": "select",
	"KeyP": "start",
	"Space": "start",
	"NumpadSubtract": "start",
	"Backspace": "button_B",
	"Numpad0": "button_B",
	"Enter": "button_A",
	"NumpadEnter": "button_A",
};

let keyboardMappings;

export function init() {
	load();
	showMappings();

	register();
}

export function register() {
	// remote with keyboard
	document.addEventListener('keydown', handle_keyDown, false);
	document.addEventListener('keyup', handle_keyUp, false);

	// remote with mouse and touch
	['mousedown', 'touchstart'].forEach(function (e) {
		container.addEventListener(e, handle_kb_mousedown, { passive: true });
	});
	['mouseup', 'touchend'].forEach(function (e) {
		container.addEventListener(e, handle_kb_mouseup, { passive: true });
	});
}

export function unregister() {
	// remote with keyboard
	document.removeEventListener('keydown', handle_keyDown, false);
	document.removeEventListener('keyup', handle_keyUp, false);

	// remote with mouse and touch
	['mousedown', 'touchstart'].forEach(function (e) {
		container.removeEventListener(e, handle_kb_mousedown);
	});
	['mouseup', 'touchend'].forEach(function (e) {
		container.removeEventListener(e, handle_kb_mouseup);
	});
}

//////////////////////////////////////////////////////////////////////
function load() {
	keyboardMappings = localStorage.getItem('mappings') ? JSON.parse(localStorage.getItem('mappings')) : defaultKeyboardMappings;
}
function save() {
	localStorage.setItem('mappings', JSON.stringify(keyboardMappings));
}
function reset() {
	localStorage.removeItem('mappings');
}

function handle_keyDown(e) {
	e.preventDefault();
	if (!autolearn) {
		if (keyboardMappings[e.code] != undefined) {
			// send command to MDT
			myWebMIDI.sendCommandOn(keyboardMappings[e.code]);
		}
	} else {
		if (e.code == "Escape") {
			// enf of edit mode
			keyedit.classList.remove('edit');
			keyedit = null;
		} else {
			// add new mapping
			keyboardMappings[e.code] = keyedit.dataset.command;
			keyedit.dataset.after = e.key;
			save();
			showMappings();
		}
	}
}
function handle_keyUp(e) {
	e.preventDefault();
	if (!autolearn) {
		if (container.querySelector(`button[data-command=${keyboardMappings[e.code]}].longpush`)) {
			myWebMIDI.sendCommandOff(keyboardMappings[e.code]);
		}
	}
}
function handle_kb_mousedown(e) {
	if (e.target.parentElement.classList.contains('mappings')) {
		// autolearn on/off
		removeClass('#keyboard .key', 'edit');
		keyboard.classList.toggle('autolearn');
		autolearn = !autolearn;
		return;
	}

	if (e.target.dataset.command) {
		if (!autolearn) {
			myWebMIDI.sendCommandOn(e.target.dataset.command);
		} else {
			if (keyedit == null || !keyedit.classList.contains('edit')) {
				// put key in edit mode
				if (keyedit != e.target) {
					removeClass('#keyboard .key.edit', 'edit');
				}
				keyedit = e.target;

				// delete all current mapping for the command to be edited
				if (!keyedit.classList.contains('edit')) {
					keyedit.classList.add('edit');
					keyedit.dataset.after = '';
					let keys = Object.keys(keyboardMappings);
					keys.forEach(k => {
						if (keyboardMappings[k] == keyedit.dataset.command) {
							delete keyboardMappings[k];
						}
					})
					showMappings();
				}
			} else {
				// enf of edit mode
				keyedit.classList.remove('edit');
				keyedit = null;
			}
		}
	}
}
function handle_kb_mouseup(e) {
	if (!autolearn && e.target.dataset.command) {
		if (e.target.classList.contains('longpush')) {
			myWebMIDI.sendCommandOff(e.target.dataset.command);
		}
	}
}


function showMappings() {
	const actions = Object.keys(myWebMIDI.MIDI_CC_commands);
	const keyzone = document.querySelector('#keymappings');
	keyzone.innerHTML = '<h4>Key mappings</h4><table><tbody></tbody></table>';
	const table = keyzone.querySelector('table');

	for (let a = 0; a < actions.length; a++) {
		let tr = document.createElement("tr")
		let arrKeys = [];
		let keyName;
		for (let i = 0; i < Object.keys(keyboardMappings).length; i++) {
			if (actions[a] === Object.values(keyboardMappings)[i]) {
				keyName = getDisplayName(Object.keys(keyboardMappings)[i]);
				arrKeys.push(keyName);
			}
		}

		// update label on key
		keyboard.querySelector(`button[data-command=${actions[a]}]`).dataset.after = keyName;

		tr.innerHTML = `<td class=map>${actions[a].toUpperCase()}</td><td>${arrKeys.join(", ")}</td>`;
		table.appendChild(tr);
	}
}

function getDisplayName(keyName) {
	return keyName.replace("Key", "")
		.replace("ShiftLeft", "Left Shift")
		.replace("Numpad", "")
		.replace("Multiply", "*")
		.replace("Subtract", "-")
		.replace("Add", "+")
		.replace("Divide", "/")
		.replace("Arrow", "Cursor ");
}
