import * as WebAudio from './webaudio.js';
import * as myWebMIDI from './webmidi.js';
import * as Keypad from './keypad.js';
import * as Display from './sketchWEBGL.js';
import * as FileManager from './filemanager.js';

import { show, hide, clear } from './utils.js';

const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);

const debug = urlParams.get('debug') || false;

const features = {
	webmidi: false,
	webserial: false,
	webaudio: false
};

let streamAudio;

// feature detection WebSerial
if ("serial" in navigator) {
	features.webserial = true;
	document.querySelector('#featureSerial').classList.add('active');
}

// feature detection WebMIDI
const { isEnabled, mySynthInput } = await myWebMIDI.init();
features.webmidi = isEnabled;

// feature detection WebAudio
features.webaudio = WebAudio.feature();

// MAIN
let btnConnection = document.querySelector('#btnConnection');

btnConnection.addEventListener('click', async (e) => {
	!btnConnection.classList.contains('connected') ? await connect() : disconnect();
})
// register_shortcut();

async function connect() {
	btnConnection.classList.toggle('connected');
	if (!features.webmidi) return;

	if (features.webserial) {
		await FileManager.init();
		show('#btnFileManager');
	}

	btnConnection.innerHTML = 'Dis<u>c</u>onnect';

	// start web audio
	if (features.webaudio) {
		streamAudio = await WebAudio.init();
	}

	// Start rendering display
	Display.init(streamAudio, mySynthInput);

	// remote controls
	Keypad.init();
	show('#renderZone, #controls, #analysers, #buttons button.action');
}

function disconnect() {
	btnConnection.classList.toggle('connected');
	// User clicked a button to close the serial port.
	// remove all listeners
	Keypad.unregister();

	btnConnection.innerHTML = '<u>C</u>onnect';
	hide('#renderZone, #controls, #analysers, #buttons button.action, #btnFileManager');

	clear('#keymappings');

	if (features.webserial) {
		FileManager.close();
	}
	Display.clear();
}

// function register_shortcut() {
// 	document.onkeyup = async function (e) {
// 		console.log('keyup', e)
// 		if (e.altKey == true && e.key == 'c') {
// 			!btnConnection.classList.contains('connected') ? await connect() : disconnect();
// 		}
// 	};
// }