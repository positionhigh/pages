let mySynthOutput;
let sequencerStarted = false;

const MIDI_CHANNEL_REMOTE = 16;

export const MIDI_CC_commands = {
	"right": 20,
	"left": 21,
	"up": 22,
	"down": 23,
	"select": 24,
	"start": 127, // not used
	"button_B": 26,
	"button_A": 27,
};

export async function init() {
	// feature detection WebMIDI
	return await WebMidi
		.enable({ sysex: true })
		.then(onEnabled)
		.catch((err) => {
			alert(err)
			console.log("WebMidi could not be enabled.", err);
			// https://jazz-soft.net/download/Jazz-Plugin/
			return false;
		}
		);

}

// Function triggered when WebMidi.js is ready
function onEnabled() {
	if (WebMidi.inputs.length < 1) {
		document.body.innerHTML += "No MIDI device detected.";

		return false;
	} else {
		// check for MicroDexed in the list
		let mySynthInput = WebMidi.getInputByName("MicroDexed")
		mySynthOutput = WebMidi.getOutputByName("MicroDexed")
		// console.log("midi enabled INPUT", mySynthInput)
		// console.log("midi enabled OUPUT", mySynthOutput)

		document.querySelector('#featureMidi').classList.add('active');

		// Synchronize with MDT sequencer
		mySynthInput.addListener("start", e => {
			sequencerStarted = true
		});
		mySynthInput.addListener("stop", e => {
			sequencerStarted = false
		});

		return { isEnabled: WebMidi.enabled, mySynthInput };
	}
}

export function sendInitRemoteDisplay() {
	mySynthOutput.sendControlChange(28, 1, MIDI_CHANNEL_REMOTE);
}
export function sendCloseRemoteDisplay() {
	mySynthOutput.sendControlChange(28, 0, MIDI_CHANNEL_REMOTE);
}

export function sendTouchDisplay(x, y) {
	// let xh = ~~(x/127),
	// 	xl = x%127,
	// 	yh = ~~(y/127),
	//  	yl = y%127;
	mySynthOutput?.sendControlChange(29, x, MIDI_CHANNEL_REMOTE);
	mySynthOutput?.sendControlChange(30, y, MIDI_CHANNEL_REMOTE);
}
export function sendTouchRelease() {
	mySynthOutput?.sendControlChange(31, 0, MIDI_CHANNEL_REMOTE);
}

export function sendCommandOn(cmd) {
	if (cmd == "start") { // MIDI realtime message
		if (!sequencerStarted) {
			sequencerStarted = true;
			mySynthOutput?.sendStart();
		} else {
			sequencerStarted = false;
			mySynthOutput?.sendStop();
		}
	} else {
		mySynthOutput?.sendControlChange(MIDI_CC_commands[cmd], 127, MIDI_CHANNEL_REMOTE);
	}
}
export function sendCommandOff(cmd) {
	mySynthOutput?.sendControlChange(MIDI_CC_commands[cmd], 0, MIDI_CHANNEL_REMOTE);
}

export function sendSysex(sysex_data) {
	mySynthOutput?.sendSysex([0x43, 0x21, 0x00], sysex_data);
}