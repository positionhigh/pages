import { getRGBColor, get8bitValue } from './utils.js';

const queryString = window.location.search;
const urlParams = new URLSearchParams(queryString);

export const scale = urlParams.get('scale') || 2;
export const WIDTH = 320 * scale;
export const HEIGHT = 240 * scale;
const offset = scale / 2;
const debug = false;

const RENDER = {
    DRAW_PIXEL: 90,
    FILL_RECT: 94,
    FILL_RECT_RAINBOW: 95,
    DRAW_LINE: 96,
    FILL_CIRCLE: 97,
    DRAW_RECT: 98,
    DRAW_STRING: 72,
}

export function renderChunk(p, data) {
    // console.log('renderChunk', data.length);
    let x, y, x2, y2, w, h, r, color, bgcolor, size, str, nbBytesColors;
    p.noSmooth();
    p.strokeCap(p.SQUARE);
    p.strokeJoin(p.MITER);
    p.noStroke();

    switch (data[0]) {
        case RENDER.DRAW_PIXEL:
            x = get8bitValue(data, 1);
            y = get8bitValue(data, 3);

            nbBytesColors = data[5];
            if (nbBytesColors > 2) {
                color = getRGBColor(data.slice(6, 10));
            } else {
                color = getRGBColor([data[6]]);
            }
            if (debug) console.log(`drawPixel x:${x} y:${y} color ${color}`)

            p.fill(color);
            p.rect(x * scale, y * scale, 1 * scale, 1 * scale);
            break;

        case RENDER.FILL_RECT:
            x = get8bitValue(data, 1);
            y = get8bitValue(data, 3);
            w = get8bitValue(data, 5);
            h = get8bitValue(data, 7);

            nbBytesColors = data[9];
            if (nbBytesColors > 2) {
                color = getRGBColor(data.slice(10, 14));
            } else {
                color = getRGBColor([data[10]]);
            }
            if (debug) console.log(`fillRect x:${x} y:${y} w:${w} h:${h} color ${color}`)

            if (w == 320 && h == 240) {
                // fill screen
                p.background(color);
            } else {
                // p.stroke(color);
                // p.strokeWeight(scale);
                p.fill(color);
                p.rect(x * scale, y * scale, w * scale, h * scale);
            }
            break;

        case RENDER.FILL_RECT_RAINBOW:
            x = get8bitValue(data, 1);
            y = get8bitValue(data, 3);
            w = get8bitValue(data, 5);
            h = get8bitValue(data, 7);

            if (debug) console.log(`fillRectRainbow ${data} x:${x} y:${y} w:${w} h:${h}`)

            // HSV colors
            p.colorMode(p.HSB, 359);
            for (let z = 0; z < h; z++) {
                if (h != 0) {
                    p.fill(100 - h + z, 200, 200); // HSV
                    p.rect(x * scale, (y + z) * scale, w * scale, 1 * scale);
                }
            }
            break;

        case RENDER.DRAW_LINE:
            x = get8bitValue(data, 1);
            y = get8bitValue(data, 3);
            x2 = get8bitValue(data, 5);
            y2 = get8bitValue(data, 7);

            nbBytesColors = data[9];
            if (nbBytesColors > 2) {
                color = getRGBColor(data.slice(10, 14));
            } else {
                color = getRGBColor([data[10]]);
            }
            if (debug) console.log(`drawLine ${data} x:${x} y:${y} x2:${x2} y2:${y2} color ${color}`)

            if (x2 == x || y2 == y) {
                if ((y2 == y && x2 != x)) {
                    p.stroke(color);
                    p.strokeWeight(scale);
                    p.line(x * scale, y * scale, x2 * scale, y2 * scale)
                } else {
                    w = x2 - x + 1;
                    h = y2 - y + 1;
                    p.fill(color);
                    p.rect(x * scale, y * scale, w * scale, h * scale)
                }
            } else {
                p.stroke(color);
                p.strokeWeight(scale);
                p.line(x * scale, y * scale, x2 * scale, y2 * scale)
            }

            break;

        case RENDER.FILL_CIRCLE:
            x = get8bitValue(data, 1);
            y = get8bitValue(data, 3);
            r = get8bitValue(data, 5);

            nbBytesColors = data[7];
            if (nbBytesColors > 2) {
                color = getRGBColor(data.slice(8, 12));
            } else {
                color = getRGBColor([data[8]]);
            }
            if (debug) console.log(`fillCircle x:${x} y:${y} r:${r} color ${color}`)

            p.fill(color);
            p.circle(x * scale, y * scale, 2 * r * scale);
            break;

        case RENDER.DRAW_RECT:
            x = get8bitValue(data, 1);
            y = get8bitValue(data, 3);
            w = get8bitValue(data, 5);
            h = get8bitValue(data, 7);

            nbBytesColors = data[9];
            if (nbBytesColors > 2) {
                color = getRGBColor(data.slice(10, 14));
            } else {
                color = getRGBColor([data[10]]);
            }
            if (debug) console.log(`drawRect x:${x} y:${y} w:${w} h:${h} color ${color}`)

            p.noFill();
            p.stroke(color);
            // p.strokeWeight(scale);
            // p.rect(x * scale, y * scale, w * scale, h * scale);
            p.strokeWeight(scale);
            p.line(x * scale, y * scale + offset, (x + w) * scale, y * scale + offset);
            p.line((x + w) * scale - offset, y * scale + offset * 2, (x + w) * scale - offset, (y + h) * scale - offset * 2);
            p.line((x + w) * scale, (y + h) * scale - offset, x * scale, (y + h) * scale - offset);
            p.line(x * scale + offset, y * scale + offset * 2, x * scale + offset, (y + h) * scale - offset) * 2;
            break;

        case RENDER.DRAW_STRING:
            x = get8bitValue(data, 1);
            y = get8bitValue(data, 3);

            nbBytesColors = data[5];
            if (nbBytesColors > 2) {
                color = getRGBColor(data.slice(6, 10));
                bgcolor = getRGBColor([data[10]]);
                size = data[11];
                str = data.slice(12, data.length);
            } else {
                color = getRGBColor([data[6]]);
                bgcolor = getRGBColor([data[7]]);
                size = data[8];
                str = data.slice(9, data.length);
            }
            if (color == bgcolor) bgcolor = "black";

            if (debug) console.log(`drawStringFont !${String.fromCharCode.apply(String, str)}! color ${color} bgcolor ${bgcolor}`)

            p.fill(bgcolor)
            p.rect(x * scale, y * scale, str.length * size * 6 * scale, 8 * scale * size);

            p.textSize(6 * scale * size);
            p.fill(color);
            p.text(String.fromCharCode.apply(String, str), x * scale, y * scale + 7 * scale * size);
            break;

        default:
            console.error('render command not supported', data);
            break;
    }

}