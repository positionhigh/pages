let reader = {};
let writer = {};
let port;
let keepReading;
let closedReadPromise;
const urlParams = new URLSearchParams(window.location.search);

const Teensy41 = {
	usbVendorId: 0x16c0,
	usbProductId: urlParams.has('dev') ? 0x048b : 0x048a
}

const td = new TextDecoder("utf-8");

export async function open() {

	// Try autoconnect
	const ports = (await navigator.serial.getPorts())
		.filter(p => p.getInfo().usbVendorId === Teensy41.usbVendorId && p.getInfo().usbProductId === Teensy41.usbProductId);
	port = ports.length === 1 ? ports[0] : null;

	if (!port) {
		// Prompt user to select a Teensy device.
		const filters = [{ usbVendorId: Teensy41.usbVendorId, usbProductId: Teensy41.usbProductId }];
		port = await navigator.serial.requestPort({ filters });
	}

	// Wait for the serial port to open.
	await port.open({
		baudRate: 503416580,
		// baudRate: 230400,
		// dataBits: 8,
		// stopBits: 1,
		// parity: 'none',
		bufferSize: 512,
		flowControlOptional: "none"
	});

	writer = port.writable.getWriter();

	keepReading = true;
	closedReadPromise = readUntilClosed();
}

async function readUntilClosed() {
	while (port.readable && keepReading) {
		reader = port.readable.getReader();

		try {
			// Listen to data coming from the serial device.					
			while (true) {
				const { value, done } = await reader.read();
				if (done) {
					// reader.cancel() has been called.
					break;
				}
				// value is a Uint8Array.
				// console.log("serial", value)
				_process(value);
			}
		} catch (error) {
			// Handle error...
		} finally {
			// Allow the serial port to be closed later.
			reader.releaseLock();
		}
	}

	await port.close();
}

export async function close() {
	keepReading = false;
	// Force reader.read() to resolve immediately and subsequently
	// call reader.releaseLock() in the loop example above.
	reader.cancel();
	writer.releaseLock()
	// writer.cancel();
	await closedReadPromise;
}

export async function sendString(c) {
	const encoder = new TextEncoder();
	const data = encoder.encode(c)
	// console.log('SENDSTRING', c + ':' + data)
	await writer.write(data);
}
export async function send(arr) {
	let data;
	data = new Uint8Array(arr);
	// console.log('SEND', data)
	await writer.write(data);
}

// window.addEventListener('beforeunload', async function (e) {
// 	// Force reader.read() to resolve immediately and subsequently
// 	// call reader.releaseLock() in the loop example above.
// 	reader.cancel();
// 	// await closedReadPromise;
// 	clear();
// });



const FRAME_START = 99;
const FRAME_END = 88;

const FILE_MANAGER = {
	START: 10,
	DIR: 11,
	GET: 12,
	GET_CHUNK: 13,
	SEND: 14,
	DELETE: 15,
	END: 20,
}

let _buffer = new Uint8Array(512);
export let _BIGbuffer = [];
window._DirContent = [];

let _i = 0;
let _frameLength = -1;
let filesize = 0;
let chunksize = 0;
let filesize_check = 0;
let transferRunning = false;
let transferType = 0;

function _process(data) {
	// console.log("_process", data)

	for (let i = 0; i < data.length; i++) {
		const b = data[i];

		switch (b) {
			case FRAME_END:
				const cmd = _buffer[1];
				// console.log("END cmd", cmd)
				switch (cmd) {

					case FILE_MANAGER.START:
						console.log("FM START")
						_frameLength = 2;
						break;
					case FILE_MANAGER.END:
						console.log("FM END")
						_frameLength = 2;

						const event = new Event(transferType == 1 ? 'fm_end_dir' : 'fm_end_get');
						document.dispatchEvent(event);

						if (transferRunning) {
							if (filesize != filesize_check) {
								console.log("DOWNLOAD PROBLEM")
							} else {
								console.log("DOWNLOAD OK")
							}
							// console.log("BIGbuffer", _BIGbuffer)

							_BIGbuffer = [];
							filesize = 0;
							filesize_check = 0;
							chunksize = 0;
							transferRunning = false;
							transferType = 0;
						}

						break;

					case FILE_MANAGER.DIR:
						transferType = 1;
						_frameLength = 2 + _buffer[4] + 3; // + 2 for filesize
						break;

					case FILE_MANAGER.GET:
						transferType = 2;
						filesize = _buffer[2];
						filesize += _buffer[3] << 8;
						filesize += _buffer[4] << 16;
						filesize += _buffer[5] << 24;
						filesize_check = 0;
						_frameLength = 2 + 4;
						// console.log('FM GET filesize', filesize)
						transferRunning = true;
						break;

					case FILE_MANAGER.GET_CHUNK:
						chunksize = _buffer[2] * 256 + _buffer[3];
						// console.log('FM GET CHUNK chunksize', chunksize)
						_frameLength = chunksize + 4;
						// console.log('FM GET CHUNK _frameLength', _frameLength)
						break;

					default:
						_frameLength = -1;
						break;
				}

				// end of frame ?
				// console.log(`_i:${_i} - _frameLength:${_frameLength}`)

				if (cmd <= FILE_MANAGER.END && _i === _frameLength && _buffer[0] === FRAME_START) { // fm
					if (cmd == FILE_MANAGER.DIR) {
						// console.log("buffer", _buffer.subarray(5, _frameLength))
						// let filename = td.decode(_buffer.subarray(5, _frameLength-4));

						let filesize = _buffer[2];
						filesize += _buffer[3] << 8;

						let filename = td.decode(_buffer.subarray(5, _frameLength));
						let pos = filename.indexOf("§§");
						let isDir = false;
						if (pos != -1) {
							// dir
							filename = filename.slice(0, pos);
							isDir = true;
						}

						_DirContent.push({ filename: filename, filesize: filesize, isDir: isDir })
					}
					else if (cmd == FILE_MANAGER.GET_CHUNK) {
						const chunk = _buffer.subarray(4, _frameLength)
						// console.log('GET chunksize', chunksize)
						// console.log(chunk)
						_BIGbuffer.push(...chunk);
						filesize_check += chunksize;
						// console.log('GET filesize_check', filesize_check)
					}
					_i = 0;
				}
				else {
					_buffer[_i++] = b;
				}
				break;

			default:
				// console.log(b)

				_buffer[_i++] = b;
				break;
		}

	}
}
